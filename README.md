# Scenic PinePhone

A (very much WIP) PinePhone shell written in Elixir using the scenic library

## Goals

- [ ] Able to run as the primary auto-started process inside of something like
  phosh, but functional as a 'secondary home screen'
- [ ] Runs full-screen
- [ ] Can perform plenty of basic functions (think 'build an RSS reader')
- [ ] Shells out to lightweight applications that run snappily for other
  purposes (web browser for instance)
- [ ] Have fun doing something that isn't payment processing or fractional CTO
  work

## Development

In general I expect this should run on desktops in a window as well as on the
pinephone in full-screen mode. I will start out working on it in windowed mode.


```sh
mix scenic.run
```
